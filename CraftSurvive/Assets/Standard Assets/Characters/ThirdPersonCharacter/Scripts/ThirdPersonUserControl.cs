using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam = null;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
        [SerializeField]
        Texture2D m_aim = null;

        public void OnGUI()
        {
            var w = Screen.width/2;
            var h = Screen.height / 2;
            GUI.DrawTexture(new Rect(w, h, 16, 16), m_aim);

            var p = new Vector3();
            var c = Camera.main;
            var e = Event.current;
            var mousePos = new Vector2();

            // Get the mouse position from Event.
            // Note that the y position from Event is inverted.
            mousePos.x = e.mousePosition.x;
            mousePos.y = c.pixelHeight - e.mousePosition.y;

            p = c.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, c.nearClipPlane));

            GUILayout.BeginArea(new Rect(20, 20, 250, 120));
            GUILayout.Label("Screen pixels: " + c.pixelWidth + ":" + c.pixelHeight);
            GUILayout.Label("Mouse position: " + mousePos);
            GUILayout.Label("World position: " + p.ToString("F3"));
            GUILayout.EndArea();
        }

        private void Start()
        {
            // get the transform of the main camera
            if (Camera.main != null)
                m_Cam = Camera.main.transform;
            else
            {
                Debug.LogWarning("Warning: no main camera found. " + 
                    "Third person character needs a Camera tagged \"MainCamera\", " + 
                    "for camera-relative controls.", gameObject);
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }
            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();
        }

        private void Update()
        {
            if (!m_Jump)
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
           
            var fire = CrossPlatformInputManager.GetButtonDown("Fire1");
            m_Character.Action(fire);
        }

        void OnAnimatorIK() //�������� � �������� (������� �� ���������)
        {
            var lookIKWeight = 1f;
            var bodyWeight = 0.6f;
            var headWeight = 0.8f;
            var eyesWeight = 1f;
            var clampWeight = 1f;

            var w = Screen.width / 2;
            var h = Screen.height / 2;
            var cursor = new Vector3(w, h, 10.0f);
            var targetPos = Camera.main.ScreenToWorldPoint(cursor);
            var dist = Vector3.Distance(targetPos, transform.position);
            Debug.Log(dist);

            var animator = GetComponent<Animator>();
            animator.SetLookAtWeight(lookIKWeight, bodyWeight, headWeight, eyesWeight, clampWeight); //����������� ����� ���� � ����������� ��� ������, ���� � ����
            animator.SetLookAtPosition(targetPos); //����������� ����, �� ������� �������
        }

        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            var h = CrossPlatformInputManager.GetAxis("Horizontal");
            var v = CrossPlatformInputManager.GetAxis("Vertical") / 2.0f;
            var crouch = Input.GetKey(KeyCode.C);

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v * m_CamForward + h * m_Cam.right;

                Debug.DrawRay(transform.position, m_CamForward, Color.red);
                Debug.DrawRay(transform.position, m_Move, Color.green);
            }
            else
                // we use world-relative directions in the case of no main camera
                m_Move = v * Vector3.forward + h * Vector3.right;
            
            // walk speed multiplier
            if (Input.GetKey(KeyCode.LeftShift))
                v *= 2.0f;

            if (Math.Abs(v) > 0.01f)
            {
                m_Character.Move(v, m_Move, crouch, m_Jump);
                m_Character.SetStrafe(0.0f);
            }
            else
            {
                m_Character.Move(v, m_CamForward, crouch, m_Jump);
                m_Character.SetStrafe(h);
            }

            m_Jump = false;
        }
    }
}
