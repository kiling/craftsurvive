﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(CharacterController))]

public class CharacterControls : MonoBehaviour
{
    [SerializeField] private float m_GravityMultiplier = 1.0f;
    [SerializeField] private float m_StickToGroundForce = 10.0f;
    [SerializeField] private float m_JumpSpeed = 10.0f;
    [SerializeField] private float m_WalkSpeed = 5.0f;
    [SerializeField] private float m_RunSpeed = 10.0f;
    [SerializeField] float m_MovingTurnSpeed = 360;
    [SerializeField] float m_StationaryTurnSpeed = 180;
    [SerializeField] private MouseLook m_MouseLook;
    [SerializeField] Texture2D m_aim = null;
    private CharacterController m_CharacterController = null;
    private Transform m_CamTransform = null;
    private Vector3 m_MoveDir = Vector3.zero;
    private bool m_Jumping = false;
    private CollisionFlags m_CollisionFlags;
    private bool m_Jump;
    private Animator m_animator;

    void Start()
    {
        m_CharacterController = GetComponent<CharacterController>();
        m_CamTransform = Camera.main.transform;
        m_MouseLook.Init(transform , m_CamTransform.transform);
        m_animator = GetComponent<Animator>();
    }

    public void OnGUI()
    {
        var w = Screen.width/2;
        var h = Screen.height / 2;
        GUI.DrawTexture(new Rect(w, h, 16, 16), m_aim);
    }

    private void Update()
    {
        m_MouseLook.LookRotation (transform, m_CamTransform);
        if (!m_Jump)
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
    }

    void OnAnimatorIK()
    {
        var lookIKWeight = 1f;
        var bodyWeight = 0.6f;
        var headWeight = 0.8f;
        var eyesWeight = 1f;
        var clampWeight = 1f;

        var w = Screen.width / 2;
        var h = Screen.height / 2;
        var cursor = new Vector3(w, h, 10.0f);
        var targetPos = Camera.main.ScreenToWorldPoint(cursor);

        Debug.DrawLine(transform.position, targetPos, Color.blue);
        var dist = Vector3.Distance(targetPos, transform.position);
        Debug.Log(dist);

        m_animator.SetLookAtWeight(lookIKWeight, bodyWeight, headWeight, eyesWeight, clampWeight);
        m_animator.SetLookAtPosition(targetPos);
    }


    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        // Read input
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");

        // set the desired speed to be walking or running
        var speed = Input.GetKey(KeyCode.LeftShift) ?  m_RunSpeed : m_WalkSpeed;
        var input = new Vector2(horizontal, vertical);
        if (input.sqrMagnitude > 1.0f)
            input.Normalize();

        // calculate move direction to pass to character
        Vector3 desiredMove;
        if (m_CamTransform != null)
        {
            // calculate camera relative direction to move:
            var camForward = Vector3.Scale(m_CamTransform.forward, new Vector3(1, 0, 1)).normalized;
            desiredMove = input.y * camForward + input.x * m_CamTransform.right;
        }
        else
            // we use world-relative directions in the case of no main camera
            desiredMove = input.y * Vector3.forward + input.x * Vector3.right;

        desiredMove = Vector3.ProjectOnPlane(desiredMove,  Vector3.up).normalized;

        //***
        m_animator.SetFloat("Forward", vertical, 0.1f, Time.deltaTime); 
        //***

        if (m_CharacterController.isGrounded)
        {
            m_MoveDir.y = -m_StickToGroundForce;

            if (m_Jump)
            {
                m_MoveDir.y = m_JumpSpeed;
                m_Jump = false;
                m_Jumping = true;
            }

            m_MoveDir.x = desiredMove.x * speed;
            m_MoveDir.z = desiredMove.z * speed;
        }
        else
        {
            m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
        }

        m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);
         m_MouseLook.UpdateCursorLock();
    }
}